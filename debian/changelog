repeatmasker-recon (1.08-8) unstable; urgency=medium

  * Team upload.
  * Add patch to add in missing include. Close: #1066541
  * Standards-Version: 4.6.2 (routine-update)

 -- Michael R. Crusoe <crusoe@debian.org>  Wed, 13 Mar 2024 16:00:34 +0100

repeatmasker-recon (1.08-7) unstable; urgency=medium

  * Fix watch file
  * Standards-Version: 4.6.0 (routine-update)
  * Respect DEB_BUILD_OPTIONS in override_dh_auto_test target (routine-
    update)
  * Remove trailing whitespace in debian/rules (routine-update)

 -- Andreas Tille <tille@debian.org>  Sat, 09 Oct 2021 08:21:33 +0200

repeatmasker-recon (1.08-6) unstable; urgency=medium

  * Fix FTCBFS by not hardcoding make in d/rules
    (Closes: #982912)
  * Drop help2man from build-deps
  * Add myself to uploaders
  * Add d/salsa-ci.yml
  * Bump watch version to 4
  * Migrate to secure https URLs
  * Declare compliance with policy 4.5.1

 -- Nilesh Patra <npatra974@gmail.com>  Tue, 16 Feb 2021 21:18:05 +0530

repeatmasker-recon (1.08-5) unstable; urgency=medium

  * Team Upload.
  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Bump debhelper from old 11 to 12.
  * Set debhelper-compat version in Build-Depends.

  [ Nilesh Patra ]
  * Add autopkgtests
  * Fix watch URL
  * compat version: 13, standards version: 4.5.0
  * Add "Rules-Requires-Root:no"
  * Update URLs

 -- Nilesh Patra <npatra974@gmail.com>  Tue, 11 Aug 2020 17:04:32 +0530

repeatmasker-recon (1.08-4) unstable; urgency=medium

  * debhelper 11
  * Point Vcs fields to salsa.debian.org
  * Standards-Version: 4.1.5
  * Fix spelling in description
  * Do not parse d/changelog
  * hardening=+all

 -- Andreas Tille <tille@debian.org>  Tue, 10 Jul 2018 11:49:40 +0200

repeatmasker-recon (1.08-3) unstable; urgency=medium

  * Add myself to Uploaders
  * Fix source URL
  * Provide better manpages
  * cme fix dpkg-control

 -- Andreas Tille <tille@debian.org>  Wed, 06 Apr 2016 12:25:19 +0200

repeatmasker-recon (1.08-2) unstable; urgency=medium

  * Team upload
  * Drop pointer to reference from long description and replace it by
    abstract text (formatted via cme fix dpkg-control which did some
    more adjustments)
  * Add citation as debian/upstream/metadata

 -- Andreas Tille <tille@debian.org>  Tue, 02 Feb 2016 15:47:47 +0100

repeatmasker-recon (1.08-1) unstable; urgency=low

  * Initial release (Closes: # 796487)

 -- Olivier Sallou <osallou@debian.org>  Sat, 22 Aug 2015 07:06:18 +0000
